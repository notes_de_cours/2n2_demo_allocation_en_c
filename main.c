
#include "demoStruct.h"




/**
 * @brief Exemple de manipulation d'un "demoStruct"
 *
 * La structure est mise sur le tas. Il faut donc la libérer manuellement.
 */
void demo1() {
    printf("Demo 1 \n");

    demoStruct *lDemoStruct;     // lDemoStruct est un pointeur, il est donc mis sur la pile.
    lDemoStruct = allocationDemoStruct();
    // dans allocationDemoStruct, il y a un calloc.
    // La structure est donc mise sur le tas.

    if(lDemoStruct) {
        remplirDemoStruct(lDemoStruct);
        afficherDemoStruct(lDemoStruct);
        libererDemoStruct(lDemoStruct);
    }
}


/**
 * @brief exemple de manipulation d'un tableau de pointeurs de demoStruct
 *
 *
 * Le tableau est mis sur le tas, il faut donc le libérer, ainsi que les
 * structures pointées par celui-ci.
 * Le tableau consiste en une série de pointeurs vers des demoStruct (voir demo7)
 */
void demo2() {
    printf("Demo 2 \n");
    demoStruct *lDemoStruct = NULL; // lDemoStruct est un pointeur, il est donc mis sur la pile.

    demoStruct **lTableauDemo = NULL; // lTableauDemo est un pointeur, il est donc mis sur la pile.
    // lTableauDemo est un pointeur vers un tableau de pointeur de demoStruct
    // l'avantage: meilleure utilisation de la mémoire car les structures "demoStruct"
    // n'ont pas besoin d'être contigues.
    // Seul les pointeurs vers ces structures doivent être contigus.

    int tailleTableauDemo =0; // tailleTableauDemo est un entier, il est donc mis sur la pile.

    char continuer[3] = "o\n";
    while(!strcmp(continuer, "o\n")) {

        printf("#1 \n");
        lDemoStruct = allocationDemoStruct();
        remplirDemoStruct(lDemoStruct);
        lTableauDemo = ajouterDansTableauPointeurDemoStruct(lTableauDemo,
                                                    &tailleTableauDemo,
                                                    lDemoStruct);
        afficherTableauPointeurDemoStruct(lTableauDemo, tailleTableauDemo);
        printf("un autre ? ");
        fgets(continuer, 3*sizeof(char),stdin);
    }

   // cette fonction libère les demoStruct pointées par le tableau
   // et ensuite libère le tableau lui-même.
   libererTableauPointeurDemoStruct(lTableauDemo, tailleTableauDemo);

}

/**
 * @brief Exemple de manipulation d'un "demoStruct" directement sur la pile
 *
 * La structure est mise sur la pile, il ne faut donc pas la libérer.
 * Mais la chaine de caractère est mise sur la pile, il faut donc la libérer.
 */
void demo3() {
    printf("Demo 3 \n");

    demoStruct lDemoStruct; // lDemoStruct est un structure qui est mise directement dans la pile.
    //lDemoStruct =  allocationDemoStruct(); Il ne faut pas faire de calloc car la structure
    // est déjà créée.

    // mais il faut quand même allouer l'espace pour le tableau de char
    allocationChampStr(&lDemoStruct);


    // if(&lDemoStruct)    il n'est pas nécessaire de vérifier si la structure existe
    //     car c'est certain qu'elle existe étant donné qu'elle est sur le tas.

    remplirDemoStruct(&lDemoStruct); // notez l'utilisation de &
    // la fonction s'attend à recevoir un pointeur, mais lDemoStruct est directement
    // un structure, il faut donc prendre son adresse avec &.
    afficherDemoStruct(&lDemoStruct);

    // la struct étant sur la pile, il ne faut pas la libérer.
    // on ne peut donc pas appeler libererDemoStruct(&lDemoStruct)
    // puisque que ca fait un free(aDemoStruct) à la fin.

    // mais il faut libérer son contenu qui est une chaine de caractère.
    free(lDemoStruct.champStr); //notez le point . au lieu de la flèche ->

}

/**
 * @brief Exemple de manipulation d'un "demoStruct" directement sur la pile via son adresse
 *
 * La structure est mise sur la pile, il ne faut donc pas la libérer.
 * Mais la chaine de caractère est mise sur la pile, il faut donc la libérer.
 */
void demo4() {
    printf("Demo 4 \n");

    demoStruct lDemoStruct; // lDemoStruct est un structure qui est mise directement dans la pile.
    demoStruct *lDemoStructAdresse = &lDemoStruct;
    //lDemoStructAdresse =  allocationDemoStruct(); ici non plus, il ne faut pas allouer
    // de la mémoire, sinon, la structure serait mise sur le tas, et lDemoStructAdresse
    // ne serait plus la même chose que lDemoStruct.

    // mais il faut quand même allouer l'espace pour le tableau de char
    allocationChampStr(lDemoStructAdresse);

    // if(lDemoStructAdresse)    il n'est pas nécessaire de vérifier si la structure existe
    //     car c'est certain qu'elle existe étant donné qu'elle est sur le tas.

    remplirDemoStruct(lDemoStructAdresse); // notez qu'on n'utilise plus le &
    afficherDemoStruct(lDemoStructAdresse);

    // la struct étant sur la pile, il ne faut pas la libérer.
    // on ne peut donc pas appeler libererDemoStruct(lDemoStructAdresse)
    // puisque que ca fait un free(aDemoStruct) à la fin.

    // mais il faut libérer son contenu qui est une chaine de caractère.
    free(lDemoStructAdresse->champStr);//notez  la flèche -> au lieu du point .
}

/**
 * @brief Exemple de manipulation d'un tableau de "demoStruct" directement sur la pile
 *
 * Les demoStruct sont mis sur la pile, il ne faut donc pas les libérer.
 * Mais la chaine de caractère est mise sur la pile, il faut donc la libérer.
 */
void demo5() {
    printf("Demo 5 \n");


    demoStruct lTableauDemoStruct[2]; // lDemoStruct est un structure qui est mise directement dans la pile.

    // mais il faut quand même allouer l'espace pour le tableau de char
    allocationChampStr(&lTableauDemoStruct[0]);
    allocationChampStr(&lTableauDemoStruct[1]);

    printf("première struct\n");
    remplirDemoStruct(&lTableauDemoStruct[0]);
    afficherDemoStruct(&lTableauDemoStruct[0]);
    printf("deuxième struct\n");
    remplirDemoStruct(&lTableauDemoStruct[1]);
    afficherDemoStruct(&lTableauDemoStruct[1]);

    // le deux demoStruct étant directement sur la pile, il ne faut pas les libérer.
    // mais il faut libérer leur chaine de caractère.
    free(lTableauDemoStruct[0].champStr); //notez les deux façons d'accéder à champStr
    free((&lTableauDemoStruct[1])->champStr);

}

/**
 * @brief Exemple de manipulation d'un demoStruct2
 *
 * La différence est que le tableau de char est directement dans la structure.
 * Il ne faut donc pas l'allouer, ni la libérer.
 *
 */
void demo6() {
    printf("Demo 6 \n");

    // étant donné que c'est le seul démo de cette structure, les allocations et libérations seront
    // faites directement ici.

    demoStruct2 *lDemoStruct = calloc(1, sizeof(demoStruct2));
    // lDemoStruct est un pointeur, il est donc mis sur la pile.
    // mais son contenue est alloué dynamiquement (calloc), et se retrouve donc sur le tas.

    lDemoStruct->champInt = 4;
    strcpy(lDemoStruct->champStr,"allo");

    printf("%s %i \n", lDemoStruct->champStr, lDemoStruct->champInt);

    //free(lDemoStruct->champStr); on ne peut pas free le tableau de char puisque qu'il n'est pas calloc.
    free(lDemoStruct);

}

/**
 * @brief exemple de manipulation d'un tableau de demoStruct
 *
 * Le tableau est mis sur le tas, il faut donc le libérer, ainsi que les
 * structures pointées par celui-ci.
 * Le tableau consiste en une série de demoStruct (voir demo2)
 */
void demo7() {
    printf("Demo 7 \n");
    int lTailleDuTableau = 0;
    demoStruct  *lTableauDemo = calloc(1, sizeof(demoStruct));
    // lTableauDemo est un pointeur vers un tableau de demoStruct
    // Ces demoStruct seront donc contigues dans le tas.
    // Cette structure réserve donc un plus gros bloc de mémoire contigue que demo 2
    for(int i = 0; i < 2; i++) {
        lTableauDemo = realloc(lTableauDemo, (lTailleDuTableau+1) * sizeof(demoStruct));
        allocationChampStr(&lTableauDemo[i]);
        if(lTableauDemo) {  // le realloc() pourrait retourner null
            lTailleDuTableau++;  //important d'incrémenter seulement si le realloc a fonctionné.
            remplirDemoStruct(&(lTableauDemo[lTailleDuTableau-1]));
            afficherDemoStruct(&(lTableauDemo[lTailleDuTableau-1]));

        } else {
            printf("allocation de mémoire n'a pas fonctionnée");
            // il ne faut par faire de free() ici
        }
    }
    for(int i = 0; i<lTailleDuTableau; i++) {
       free(lTableauDemo[i].champStr);
    }
    free(lTableauDemo);
}

/**
 * @brief exemple de manipulation d'un tableau de demoStruct via une fonction
 *
 * * ATTENTION : Cette technique peut-être dangereuse. Il est préférable d'utiliser demo9
 *
 * La différence avec le démo7, c'est que le tableau est manipulé dans une fonction au
 * lieu de l'être directement dans la fonction qui l'a créé.
 *
 * Le tableau est mis sur le tas, il faut donc le libérer, ainsi que les
 * structures pointées par celui-ci.
 * Le tableau consiste en une série de demoStruct (voir demo2)
 */
void demo8() {
    printf("Demo 8 \n");
    int lTailleDuTableau = 0;
    demoStruct *lDemoStruct;
    demoStruct *lTableauDemo = calloc(1, sizeof(demoStruct));
    // lTableauDemo est un pointeur vers un tableau de demoStruct
    // Ces demoStruct seront donc contigues dans le tas.
    // Cette structure réserve donc un plus gros bloc de mémoire contigue que demo 2
    for(int i = 0; i < 3; i++) {
        lDemoStruct = allocationDemoStruct();
        remplirDemoStruct(lDemoStruct);

        lTableauDemo = ajouterDansTableauDemoStruct(lTableauDemo, &lTailleDuTableau, lDemoStruct);

        //free(lDemoStruct->champStr); //enlever le commentaire afin de voir l'effet du
        // double référencement.
        // À noter que si demoStruct2 avait été utilisée, cette technique fonctionnerait puisqu'il
        // n'y aurait aucune allocation dynamique du tableau de char puisqu'il est pré-alloué
        // dans la définition de la structure avec  champStr[x]
        // Ce tableau de char serait copié (memcpy) lors du =

        free(lDemoStruct); // lDemoStruct peut être détruit car il a été copié dans le tableau.

        afficherDemoStruct(&(lTableauDemo[0]));
    }
    afficherTableauDemoStruct(lTableauDemo, lTailleDuTableau);
    for(int i = 0; i < lTailleDuTableau; i++) {
       free(lTableauDemo[i].champStr);
    }
    free(lTableauDemo);
}

/**
 * @brief exemple de manipulation d'un tableau de demoStruct via une fonction
 *
 *
 * La différence avec le démo7, c'est que le tableau est manipulé dans une fonction au
 * lieu de l'être directement dans la fonction qui l'a créé.
 * L'image mémoire est la même que le demo7
 *
 * La différence avec le demo8, c'est que les demoStruct sont construit dans la
 * fonction pour ajouter dans le tableau au lieu d'être créées ici et passées en
 * paramètre. Cette facon de faire est plus sécure car elle ne nécessite pas de
 * copy de demoStruct avec le = (voir ajouterDansTableauDemoStruct pour plus d'explication)
 *
 * Le dessin est le même que pour demo7.
 *
 * Le tableau est mis sur le tas, il faut donc le libérer, ainsi que les
 * structures pointées par celui-ci.
 * Le tableau consiste en une série de demoStruct
 */
void demo9() {
    printf("Demo 9 \n");
    int lTailleDuTableau = 0;
    demoStruct *lTableauDemo = calloc(1, sizeof(demoStruct));
    // lTableauDemo est un pointeur vers un tableau de demoStruct
    // Ces demoStruct seront donc contigues dans le tas.
    // Cette structure réserve donc un plus gros bloc de mémoire contigue que demo 2
    for(int i = 0; i < 3; i++) {
        lTableauDemo = ajouterDansTableauDemoStructMeilleur(lTableauDemo, &lTailleDuTableau);
        afficherDemoStruct(&(lTableauDemo[0]));
    }
    afficherTableauDemoStruct(lTableauDemo, lTailleDuTableau);
    for(int i = 0; i < lTailleDuTableau; i++) {
       free(lTableauDemo[i].champStr);
    }
    free(lTableauDemo);
}




int main()
{

    //demo1();
    demo2();
//    demo3();
//    demo4();
//    demo5();
//    demo6();
//    demo7();
//    demo8();
//    demo9();
    printf("terminé \n");
    return 0;
}
