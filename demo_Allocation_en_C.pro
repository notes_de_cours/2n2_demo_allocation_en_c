TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        demoStruct.c \
        main.c

HEADERS += \
    demoStruct.h

DISTFILES += \
    README.md
