#ifndef DEMOSTRUCT_H
#define DEMOSTRUCT_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAILLE_CHAINE 5  //taille d'un tableau de char "standard"

typedef struct {
     int champInt;       //un entier
     char *champStr;     //un pointeur vers un tableau de char
} demoStruct;

typedef struct {
     int champInt;       //un entier
     char champStr[TAILLE_CHAINE];     //un tableau de char
} demoStruct2;

/*****
 * fonctions pour manipuler un demoStruct
 *****/
void remplirDemoStruct(demoStruct *aDemoStruct);
void allocationChampStr(demoStruct *aDemoStruct);
demoStruct *allocationDemoStruct();
void afficherDemoStruct(demoStruct *aDemoStruct);
void libererDemoStruct(demoStruct *aDemoStruct);

/*****
 * fonctions pour manipuler un tableau de pointeurs de demoStruct
 *****/

demoStruct **ajouterDansTableauPointeurDemoStruct(demoStruct **aTableauDemo,
                                                  int *aTailleTableauDemo,
                                                  demoStruct *aDemoStruct);
void afficherTableauPointeurDemoStruct(demoStruct **aTableauDemo, int aTailleTableauDemo);
void libererTableauPointeurDemoStruct(demoStruct **aTableauDemo, int aTailleTableauDemo);

/*****
 * fonctions pour manipuler un tableau de demoStruct
 *****/
demoStruct* ajouterDansTableauDemoStruct(demoStruct *aTableauDemo,
                                         int *aTailleTableauDemo,
                                         demoStruct *aDemoStruct);
demoStruct* ajouterDansTableauDemoStructMeilleur(demoStruct *aTableauDemo,
                                         int *aTailleTableauDemo);
void afficherTableauDemoStruct(demoStruct *aTableauDemo, int aTailleTableauDemo);
void libererTableauDemoStruct(demoStruct *aTableauDemo, int aTailleTableauDemo);


#endif // DEMOSTRUCT_H
