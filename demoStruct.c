#include "demoStruct.h"

/*************
 Les fonctions suivantes servent à manipuler UN demoStruct
*************/

/**
 * @brief demo de l'insertion de données dans une demoStruct
 *
 * Le contenue de la structure sera changé, mais pas le pointeur
 * vers la structure.
 * @param aDemoStruct la demoStruct à remplir.
 */
void remplirDemoStruct(demoStruct *aDemoStruct) {
    char *lOK = NULL;
    printf("Entrez une chaine de caractères : ");
    lOK = fgets(aDemoStruct->champStr, TAILLE_CHAINE * sizeof(char), stdin);
    if(lOK) {
        // fgets insère les caractères de EOL. Cette ligne les enlève en
        // insérant un char 0 à la position du premier \n, indiquant la fin de la chaine.
        aDemoStruct->champStr[strcspn(aDemoStruct->champStr, "\n")] = 0;

        char temp[TAILLE_CHAINE]; // variable temporaire requise pour faire la conversion
        printf("Entrez une nombre entier : ");
        lOK = fgets(temp, TAILLE_CHAINE * sizeof(char), stdin);
        if(lOK) {
            temp[strcspn(temp, "\n")] = 0;
            aDemoStruct->champInt = strtol(temp,NULL,10);
        }
    }

    if(!lOK) {
        printf("Une erreur s'est produite!");
    }
};


/**
 * @brief allocation de la mémoire pour entreposer le char * de demoStruct
 *
 */
void allocationChampStr(demoStruct *aDemoStruct) {
    char *tempChampStr = calloc(TAILLE_CHAINE, sizeof(char));
    aDemoStruct->champStr = tempChampStr;
}

/**
 * @brief allocation de la mémoire pour entreposer un demoStruct
 * @return le pointeur vers un nouveau demoStruct.
 */
demoStruct *allocationDemoStruct() {
    //cette allocation pourrait être fait directement dans la fonction appelante.
    //mais afin de garder la constance dans les démos, elle est utilisée.
    demoStruct *lDemoStruct = calloc(1, sizeof(demoStruct));
    allocationChampStr(lDemoStruct);
    return lDemoStruct;
}


/**
 * @brief affichage d'une demoStruct
 * @param aDemoStruct  la structure à afficher
 */
void afficherDemoStruct(demoStruct *aDemoStruct) {
    // 2 façons d'afficher une structure. La première est recommandée.
    printf("%s %i \n", aDemoStruct->champStr, aDemoStruct->champInt);
    //printf("%s %i \n", (*aDemoStruct).champ1, (*aDemoStruct).champ3);
}

/**
 * @brief libération de la mémoire occupée par le contenue de la demoStruct
 * @param aDemoStruct la structure à libérer
 */
void libererDemoStruct(demoStruct *aDemoStruct) {
    free(aDemoStruct->champStr); //libère le tableau de char
    //pas besoin de libérer le int.
    free(aDemoStruct);
}


/***********
 * fonctions pour manipuler un tableau de pointeurs de demoStruct
 ***********/

/**
 * @brief ajout d'un demoStruct dans un tableau de pointeur de demostruct
 *
 * La nouvelle info est inséré à la fin du tableau.
 * Si le tableau est vide, un calloc initial est fait
 * Si le tableau a déjà des données, un realloc est fait pour ajouter la nouvelle donnée
 * aTailleTableauDemo sera incrémenté en sortant de cette fonction
 *
 * @param aTableauDemo le tableau de pointeurs de demoStruct dans lequel ajouter
 * @param aTailleTableauDemo la taille actuelle du tableau.
 * @param aDemoStruct la structure à ajouter
 * @return l'adresse du tableau de pointeurs de demoStruct.
 * Peut être l'original, ou une nouvelle.
 */
demoStruct **ajouterDansTableauPointeurDemoStruct(demoStruct **aTableauDemo,
                                                  int *aTailleTableauDemo,
                                                  demoStruct *aDemoStruct) {
    demoStruct **lResultat = NULL;
    if(!aTableauDemo) { // pointeur null
        lResultat = calloc(1,sizeof(demoStruct*));
    } else {
        lResultat = realloc(aTableauDemo, (*aTailleTableauDemo+1) * sizeof(demoStruct*));

    }
    if (lResultat) {
        lResultat[*aTailleTableauDemo]= aDemoStruct;
        *aTailleTableauDemo += 1;
    } else {
        printf("La mémoire n'a pu être allouée pour ajouter un demo! \n");
    }

    return lResultat;
};



/**
 * @brief affichage du tableau de pointeur au complet
 * @param aTableauDemo le tableau de pointeur de demoStruct à afficher
 * @param aTailleTableauDemo la taille du tableau.
 */
void afficherTableauPointeurDemoStruct(demoStruct **aTableauDemo, int aTailleTableauDemo) {
    for (int i = 0; i<aTailleTableauDemo; i++) {
        printf("%i ",i);
        afficherDemoStruct(aTableauDemo[i]);
    }
};

/**
 * @brief libération d'un tableau de pointeurs de demoStruct.
 *
 * Les demoStruct pointées par ce tableau sont aussi libérées
 * @param aTableauDemo le tableau de pointeurs de demoStruct
 * @param aTailleTableauDemo la taille du tableau.
 */
void libererTableauPointeurDemoStruct(demoStruct **aTableauDemo, int aTailleTableauDemo) {
    for(int i = 0; i < aTailleTableauDemo; i++) {
        libererDemoStruct(aTableauDemo[i]);
    }
    free(aTableauDemo);
}

/***********
 * fonctions pour manipuler un tableau de demoStruct
 ***********/

/**
 * @brief ajout d'un demoStruct dans un tableau  de demostruct
 *
 * ATTENTION : Cette technique peut-être dangereuse.
 * Il est préférable d'utiliser demo9 et ajouterDansTableauDemoStructMeilleur
 *
 * Si le tableau est null, un calloc initial est fait
 * Si le tableau contient déjà des données, un realloc est fait pour ajouter
 * la nouvelle structure aTailleTableauDemo sera incrémenté en sortant de
 * cette fonction si l'allocation a réussie
 *
 * @param aTableauDemo le tableau  de demoStruct dans lequel ajouter
 * @param aTailleTableauDemo la taille actuelle du tableau.
 * @param aDemoStruct le demoStruct à insérer
 * @return l'adresse du tableau de demoStruct. Peut être l'original, ou une nouvelle.
 */
demoStruct* ajouterDansTableauDemoStruct(demoStruct *aTableauDemo,
                                         int *aTailleTableauDemo,
                                         demoStruct *aDemoStruct) {
    demoStruct *lResultat = NULL;
    if(!aTableauDemo) { // pointeur null
        lResultat = calloc(1,sizeof(demoStruct));
    } else {
        lResultat = realloc(aTableauDemo, (*aTailleTableauDemo+1) * sizeof(demoStruct));
    }
    if (lResultat) {
        lResultat[*aTailleTableauDemo] = *aDemoStruct;
        // Attention: cette assignation est dangereuse
        // http://blog.zhangliaoyuan.com/blog/2013/01/28/structure-assignment-and-its-pitfall-in-C-language/
        *aTailleTableauDemo += 1;

    } else {
        printf("La mémoire n'a pu être allouée pour ajouter un demo! \n");
    }

    return lResultat;
};


/**
 * @brief ajout d'un demoStruct dans un tableau  de demostruct
 *
 * Meilleur technique pour insérer un struct dans un tableau de struct.
 * Au lieu de recevoir la struct en paramètre et de la "copier" dans le tableau,
 * elle est construite directement dans le tableau.
 *
 * Si le tableau est null, un calloc initial est fait
 * Si le tableau contient déjà des données, un realloc est fait pour ajouter
 * la nouvelle structure aTailleTableauDemo sera incrémenté en sortant de
 * cette fonction si l'allocation a réussie
 *
 * @param aTableauDemo le tableau  de demoStruct dans lequel ajouter
 * @param aTailleTableauDemo la taille actuelle du tableau.
 * @return l'adresse du tableau de demoStruct. Peut être l'original, ou une nouvelle.
 */
demoStruct* ajouterDansTableauDemoStructMeilleur(demoStruct *aTableauDemo,
                                         int *aTailleTableauDemo) {
    demoStruct *lResultat = NULL;
    if(!aTableauDemo) { // pointeur null
        lResultat = calloc(1,sizeof(demoStruct));
    } else {
        lResultat = realloc(aTableauDemo, (*aTailleTableauDemo+1) * sizeof(demoStruct));
    }
    if (lResultat) {
        allocationChampStr(&lResultat[*aTailleTableauDemo]);
        remplirDemoStruct(&lResultat[*aTailleTableauDemo]); //la différence avec la
        // fonction précédente est le fait que la struct est remplie directement
        // dans le tableau.

        *aTailleTableauDemo += 1;
    } else {
        printf("La mémoire n'a pu être allouée pour ajouter un demo! \n");
    }

    return lResultat;
};


/**
 * @brief affichage du tableau de demoStruct au complet
 * @param aTableauDemo le tableau de demoStruct à afficher
 * @param aTailleTableauDemo la taille du tableau.
 */
void afficherTableauDemoStruct(demoStruct *aTableauDemo, int aTailleTableauDemo) {
    for (int i = 0; i<aTailleTableauDemo; i++) {
        printf("%i ",i);
        afficherDemoStruct(&aTableauDemo[i]);
    }
};

/**
 * @brief libération d'un tableau de demoStruct.
 *
 * @param aTableauDemo le tableau de demoStruct
 * @param aTailleTableauDemo la taille du tableau.
 */
void libererTableauDemoStruct(demoStruct *aTableauDemo, int aTailleTableauDemo) {
    for(int i = 0; i < aTailleTableauDemo; i++) {
        free(aTableauDemo[i].champStr); //libération des champStr
    }
    free(aTableauDemo); //libération du tableau
}




